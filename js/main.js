var DSSV = "DSSV_LOCAL";
var dssv = [];

getLocalStorage();

function setLocalStorage() {
  var dataJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV, dataJson);
  renderDSSV(dssv);
}

function getLocalStorage() {
  var dataJson = localStorage.getItem(DSSV);
  if (dataJson != null) {
    var svArr = JSON.parse(dataJson);
    dssv = svArr.map(function (item) {
      var sv = new SinhVien(
        item.maSV,
        item.tenSV,
        item.emailSV,
        item.matKhauSV,
        item.diemToan,
        item.diemLy,
        item.diemHoa
      );
      return sv;
    });
    renderDSSV(dssv);
  }
}

function themSinhVien() {
  var sv = layThongTinTuForm();
  dssv.push(sv);
  setLocalStorage();
}

function xoaSinhVien(idSV) {
  var viTri = timKiemViTri(idSV, dssv);
  if (viTri != -1) {
    dssv.splice(viTri, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(idSV) {
  var viTri = timKiemViTri(idSV, dssv);
  if (viTri == -1) {
    return;
  }
  var sv = dssv[viTri];
  hienThiThongTinLenForm(sv);
  getID("txtMaSV").disabled = true;
}

function capNhatSinhVien() {
  var sv = layThongTinTuForm();
  var viTri = timKiemViTri(sv.maSV, dssv);
  if (viTri != -1) {
    dssv[viTri] = sv;
    renderDSSV(dssv);
    setLocalStorage();
  }
}

function resetSinhVien() {
  resetForm();
}

// function timKiemSinhVien() {
//   var timKiem = getID("txtTimKiem").value;
// }
