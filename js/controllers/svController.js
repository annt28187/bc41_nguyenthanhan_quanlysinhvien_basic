function getID(x) {
  return document.getElementById(x);
}

function layThongTinTuForm() {
  var _maSV = getID("txtMaSV").value;
  var _tenSV = getID("txtTenSV").value;
  var _emailSV = getID("txtEmailSV").value;
  var _matKhauSV = getID("txtMatKhauSV").value;
  var _diemToan = getID("txtDiemToan").value * 1;
  var _diemLy = getID("txtDiemLy").value * 1;
  var _diemHoa = getID("txtDiemHoa").value * 1;
  var sv = new SinhVien(
    _maSV,
    _tenSV,
    _emailSV,
    _matKhauSV,
    _diemToan,
    _diemLy,
    _diemHoa
  );
  return sv;
}

function renderDSSV(svArr) {
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    var contentTr = `<tr>
                  <td>${sv.maSV}</td>
                  <td>${sv.tenSV}</td>
                  <td>${sv.emailSV}</td>
                  <td>${sv.tinhDTB()}</td>
                  <td><button class="btn btn-danger" onclick="xoaSinhVien('${
                    sv.maSV
                  }')">Xóa</button></td>
                  <td><button class="btn btn-warning" onclick="suaSinhVien('${
                    sv.maSV
                  }')">Sửa</button></td>
          </tr>`;
    contentHTML += contentTr;
  }
  getID("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(idSV, svArr) {
  var viTri = -1;
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    if (sv.maSV == idSV) {
      viTri = i;
      break;
    }
  }
  return viTri;
}

function hienThiThongTinLenForm(sv) {
  getID("txtMaSV").value = sv.maSV;
  getID("txtTenSV").value = sv.tenSV;
  getID("txtEmailSV").value = sv.emailSV;
  getID("txtMatKhauSV").value = sv.matKhauSV;
  getID("txtDiemToan").value = sv.diemToan;
  getID("txtDiemLy").value = sv.diemLy;
  getID("txtDiemHoa").value = sv.diemHoa;
}

function resetForm() {
  getID("formQLSV").reset();
}
